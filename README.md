<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    android:background="@color/blue"
    android:padding="@dimen/activity_padding"
    tools:context="com.example.ka.baitap_android.MainActivity">

    <ImageView
        android:id="@+id/image_View"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="@drawable/image_facebook"/>
    <EditText
        android:id="@+id/edt_Email"
        android:layout_marginBottom="@dimen/margin_Bottom"
        android:padding="@dimen/text_padding"
        android:background="@color/white"
        android:hint="@string/hint_email_phone"
        android:textColor="@color/black"
        android:layout_width="match_parent"
        android:layout_height="wrap_content" />

    <EditText
        android:id="@+id/edt_Password"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginBottom="@dimen/margin_Bottom"
        android:background="@color/white"
        android:hint="@string/hint_pass"
        android:padding="@dimen/text_padding"
        android:textColor="@color/black" />
    <Button
        android:id="@+id/bt_Log_in"
        android:textSize="@dimen/text_size_large"
        android:textColor="@color/white"
        android:text="@string/text_login"
        android:background="@color/blue_dark"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textStyle="bold"/>
    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <TextView
            android:id="@+id/tv_Sign_up"
            android:layout_centerHorizontal="true"
            android:layout_alignParentBottom="true"
            android:textColor="@color/white"
            android:text="@string/text_sign_up"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content" />

        <Button
            android:id="@+id/bt"
            android:layout_alignParentBottom="true"
            android:layout_alignParentRight="true"
            android:textColor="@color/white"
            android:text="?"
            android:background="@color/blue_dark"
            android:layout_width="20dp"
            android:layout_height="20dp" />

    </RelativeLayout>
</LinearLayout>
